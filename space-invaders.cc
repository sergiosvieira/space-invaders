#include <iostream>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <SDL.h>

const int W = 640;
const int H = 700;

class GObject {
private:
	SDL_Surface* load(std::string fn) {
		SDL_Surface* tmp = SDL_LoadBMP(fn.c_str());
		if (tmp == NULL) {
			exit(1);
		}
		return tmp;
	}
public:
	Uint8 cframe;
	SDL_Surface* image;
	SDL_Rect pos, frames[2];
	bool live;
	GObject(std::string fn, Uint16 x, Uint16 y) {
		image = load(fn);
		pos.x = x;
		pos.y = y;
		live = true;
		cframe = 0;
        frames[0].x = 0;
        frames[0].y = 0;
        frames[0].w = image->clip_rect.w;
        frames[0].h = image->clip_rect.h;
	}
	void draw(SDL_Surface* screen) {
		if (live) {
			SDL_BlitSurface(image, &frames[cframe], screen, &pos);
		}
	}
	bool collision(GObject* obj) {
		if (!obj->live || !live) return false;
		Uint16 t1 = pos.y;
		Uint16 t2 = obj->pos.y;
		Uint16 b1 = pos.y + image->clip_rect.h;
		Uint16 b2 = obj->pos.y + obj->image->clip_rect.h;
		Uint16 l1 = pos.x;
		Uint16 l2 = obj->pos.x;
		Uint16 r1 = pos.x + image->clip_rect.w;
		Uint16 r2 = obj->pos.x + obj->image->clip_rect.w;

		if (b1 < t2) return false;
		if (t1 > b2) return false;
		if (r1 < l2) return false;
		if (l1 > r2) return false;

		return true;

	}

	void die() {
		live = false;
	}

	~GObject() {
		SDL_FreeSurface(image);
	}

};

class Shoot: public GObject {
private:
	bool up;
public:
	Shoot(std::string fn, Uint16 x, Uint16 y, bool up = true) :
			GObject(fn, x, y) {
		this->up = up;
	}
	void move() {
		if (live) {
			if (up) {
				if (pos.y - image->clip_rect.w > 0) {
					pos.y -= 2;
				} else {
					live = false;
				}
			} else {
				if (pos.y - image->clip_rect.w < H) {
					pos.y += 2;
				} else {
					live = false;
				}
			}
		}
	}
};

class Player: public GObject {
public:
	Shoot* shoot;
	Player(std::string fn, Uint16 x, Uint16 y) :
			GObject(fn, x, y) {
		shoot = new Shoot("gfx/pshoot.bmp", 0, 0);
		shoot->live = false;
	}

	void draw(SDL_Surface* screen) {
		if (live)
			SDL_BlitSurface(image, &frames[cframe], screen, &pos);
		shoot->draw(screen);
	}

	void move() {
		if (live) {
			Uint8 *ks = SDL_GetKeyState(NULL);
			if (ks[SDLK_LEFT]) {
				pos.x -= 2;
				if (pos.x < 0) {
					pos.x = 0;
				}
			} else if (ks[SDLK_RIGHT]) {
				pos.x += 2;
				if (pos.x + image->clip_rect.w > W) {
					pos.x = W - image->clip_rect.w;
				}
			}
			if (ks[SDLK_SPACE]) {
				if (shoot->live == false) {
					shoot->live = true;
					shoot->pos.x = pos.x + image->clip_rect.w / 2;
					shoot->pos.y = pos.y;
				}
			}
		}
		shoot->move();
	}
};

class Invader: public GObject {
private:
	Uint8 steps, max, step;
	bool left;
	Uint32 start;
	float fps;
public:
	Shoot* shoot;
	Invader(std::string fn, Uint16 x, Uint16 y, Uint16 w, Uint16 h) :
			GObject(fn, x, y) {
		shoot = new Shoot("gfx/eshoot.bmp", 0, 0, false);
		shoot->live = false;
		steps = 4;
		left = true;
		cframe = 0;
		
        frames[0].x = 0; frames[0].y = 0; frames[0].w = w; frames[0].h = h;
        frames[1].x = w; frames[0].y = 0; frames[0].w = w; frames[0].h = h;

		max = 8;
		fps = 1000 / 4;
		start = SDL_GetTicks();
		step = 10;
	}
	void draw(SDL_Surface* screen) {
		if (live)
			SDL_BlitSurface(image, &frames[cframe], screen, &pos);
		shoot->draw(screen);
	}
	void move() {
		if (live && SDL_GetTicks() - start > fps) {
			start = SDL_GetTicks();
			if (cframe < 1) {
				cframe++;
			} else {
				cframe = 0;
			}
			if (left) {
				if (steps > max) {
					left = false;
					steps = 0;
					pos.y += step;
				} else {
					pos.x -= step;
					steps++;
				}
			} else {
				if (steps > max) {
					left = true;
					steps = 0;
					pos.y += step;
				} else {
					pos.x += step;
					steps++;
				}
			}
		}
		shoot->move();
	}
	void prepareShoot() {
		shoot->live = true;
		shoot->pos.x = pos.x + image->clip_rect.w / 2;
		shoot->pos.y = pos.y + image->clip_rect.h;
	}
};

int main(int argc, char** argv) {
	SDL_Event event;
	SDL_Surface* screen = NULL;
	bool done = false;
	SDL_Init(SDL_INIT_VIDEO);
	screen = SDL_SetVideoMode(W, H, 16, SDL_SWSURFACE);
	SDL_WM_SetCaption("FFGP - Space Invaders - sergiosvieira@gmail.com", NULL);

	Player* pl = new Player("gfx/player.bmp", 0, 0);
	pl->pos.x = (W - pl->image->clip_rect.w) / 2;
	pl->pos.y = (H - pl->image->clip_rect.h) - 5;

	Invader* inv[22];
	for (int i = 0; i < 22; i++) {
		if (i < 11) {
			inv[i] = new Invader("gfx/invader02.bmp", (i+1)*50, 64, 33, 24);
		} else {
			inv[i] = new Invader("gfx/invader03.bmp", (i+1-11)*50, 100, 35, 24);
		}
	}

	const int TPS = 60;
	const int ST = 1000 / TPS;
	std::cout << "ST: " << ST <<std::endl;
	const int MFS = 5;
	int l;
	Uint32 ngt = SDL_GetTicks();
	Invader* p = NULL;
	std::cout << "ngt: " << ngt << std::endl;
	while(!done) {
		if (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				done = true;
			}
		}
		l = 0;
		std::cout << "T: " << SDL_GetTicks() << std::endl;
		while (SDL_GetTicks() > ngt && l < MFS) {
			ngt += ST;
			l++;
			std::cout << "ngt: " << ngt << " l: " << l <<std::endl;
		}
		std::cout << "ngt: " << ngt << " l: " << l <<std::endl;
		if (pl->live) {
			pl->move();
			for (int i = 0; i < 22; i++) {
				inv[i]->move();
				if (!inv[i]->live) continue;
				if(pl->shoot->collision(inv[i])) {
					inv[i]->shoot->die();
					inv[i]->die();
					pl->shoot->die();
				}
				if(inv[i]->shoot->collision(pl)) {
					inv[i]->shoot->die();
					pl->die();
				}
			}
			if (p != NULL) {
				if (!p->shoot->live) {
					p = NULL;
				}
			}
			SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
			pl->draw(screen);
			int r = rand() % 21;
			for (int i = 0; i < 22; i++) {
				inv[i]->draw(screen);
				if (p == NULL && r == i && inv[i]->live) {
					inv[i]->prepareShoot();
					p = inv[i];
				}
			}
		}
		SDL_Flip(screen);
	}


	return 0;
}
